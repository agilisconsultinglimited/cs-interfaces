﻿using System;

namespace InterfacesExample.ThirdParty
{
    public class Employee : IPrintable, IStoreable
	{
		public Employee()
		{
		}


		public string Id { get; set; }
		public String Name { get; set; }
		public String Address { get; set; }


		public void Save()
		{
			Console.WriteLine("Employee saved.");
            Console.WriteLine();
		}


		public void Print()
		{
			Console.WriteLine("EMPLOYEE");
			Console.WriteLine("Name:    {0}", Name);
			Console.WriteLine("Address: {0}", Address);
            Console.WriteLine();
		}


	}
}
