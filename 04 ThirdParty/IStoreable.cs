﻿using System;
namespace InterfacesExample.ThirdParty
{
	public interface IStoreable
	{
		string Id { get; set; }

		void Save();
	}
}
