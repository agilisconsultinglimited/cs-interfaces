﻿using System;
namespace InterfacesExample.ThirdParty
{
    public class Printer
    {
        public Printer()
        {
        }

        public void Print(IPrintable o) {
            if (o != null)
            {
                o.Print();
            }
        }
    }
}
