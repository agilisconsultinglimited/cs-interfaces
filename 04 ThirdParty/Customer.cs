﻿using System;

namespace InterfacesExample.ThirdParty
{
    public class Customer: IPrintable, IStoreable
    {
        public Customer()
        {
        }


        public string Id { get; set; }
		public String Code { get; set; }
		public String Name { get; set; }
		public String Address { get; set; }


        public void Save()
        {
            Console.WriteLine("Customer saved.");
            Console.WriteLine();
        }


        public void Print()
        {
            Console.WriteLine("CUSTOMER");
            Console.WriteLine("Code:    {0}", Code);
			Console.WriteLine("Name:    {0}", Name);
			Console.WriteLine("Address: {0}", Address);
            Console.WriteLine();
		}


        public void Validate()
        {
            Console.WriteLine("Customer validated.");
        }


	}
}
