﻿using System;

namespace InterfacesExample.ThirdParty
{
	public interface IPrintable
	{
		void Print();
	}
}
