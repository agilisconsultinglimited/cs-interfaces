﻿using System;

namespace SomeoneElsesLibrary
{
    /// <summary>
    /// This is a third-party class, from someone elses code.
    /// Maybe you even don't have a code, just a DLL assembly
    /// added to your application, so you can't change the code at all.
    /// 
    /// Worse, the creator doesn't allow to inherit from this class
    /// by applying `sealed` keyword, so that nobody messes up with his creation.
    /// Now, how to make this class printable and saveable within our app,
    /// just like our own classes such as Customer and Employee are?
    /// 
    /// </summary>
    public sealed class Product
    {
        public Product()
        {
        }

        public string Name { get; set; }
        public string Code { get; set; }
        public double Quantity { get; set; }

    }
}
