﻿using System;

namespace InterfacesExample.ThirdParty
{
    /// <summary>
    /// Let's make that third-party class "ours", by wrapping it 
    /// in our own thin wrapper, which only adds ability to print and to store it.
    /// </summary>
    public class Product : IPrintable, IStoreable
    {
        public Product(SomeoneElsesLibrary.Product data)
        {
            Data = data;
        }

        public SomeoneElsesLibrary.Product Data { get; set; }

        /// <summary>
        /// Let's simulate ID property, required by IStoreable
        /// We'll simply recycle SomeoneElsesLibrary.Product.Code for that.
        /// </summary>
        public string Id 
        { 
            get 
            {
                return Data.Code;
            } 
            set
            {
                Data.Code = value;
            }
        }


		public void Print()
        {
            Console.WriteLine("THIRD PARTY PRODUCT");
            Console.WriteLine("Code:     {0}", Data.Code);
            Console.WriteLine("Name:     {0}", Data.Name);
            Console.WriteLine("Quantity: {0}", Data.Quantity);
            Console.WriteLine();
		}

        public void Save()
        {
            if (Data != null)
            {
                Console.WriteLine("Third party product saved.");
                Console.WriteLine();
            }
		}
    }
}
