﻿using System;
namespace InterfacesExample.Interfaces
{
    public class Printer
    {
        public Printer()
        {
        }

        public void Print(IDataObject o) {
            if (o != null)
            {
                o.Print();
            }
        }
    }
}
