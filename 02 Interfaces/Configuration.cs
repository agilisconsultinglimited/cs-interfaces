﻿using System;
using System.Collections.Generic;


namespace InterfacesExample.Interfaces
{
	public class Configuration : List<string>, IDataObject
	{
		public Configuration()
		{
		}

        public string Id { get; set; }
		public bool FullScreen { get; set; }
		public string LastFolder { get; set; }


		public void Print()
		{
			// Well, I don't need to be able to print configuration :-( 
			// Yet I still have to implement it, because IDataObject 
			// interface dictates so
		}


		public void Save()
		{
			Console.WriteLine("Configuration saved.");
			Console.WriteLine();
		}
	}
}
