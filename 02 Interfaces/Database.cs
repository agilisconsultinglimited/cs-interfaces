﻿using System;
namespace InterfacesExample.Interfaces
{
    public class Database
    {
        public Database()
        {
        }

        public void Save(IDataObject o) {
            if (o != null) {
                o.Save();
            }
        }
    }
}
