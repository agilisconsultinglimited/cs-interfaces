﻿using System;

namespace InterfacesExample.Interfaces
{
    /// <summary>
    /// Generic data object
    /// </summary>
    public class DataObject
    {
        public virtual void Save() {
            throw new NotImplementedException();
        }


		public virtual void Print()
		{
            throw new NotImplementedException();
		}

	}
}
