﻿using System;
namespace InterfacesExample.Interfaces
{
    public interface IDataObject
    {
		string Id { get; set; }
		
        void Save();

        void Print();
	}
}
