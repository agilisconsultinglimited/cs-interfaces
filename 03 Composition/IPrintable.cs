﻿using System;
namespace InterfacesExample.Composition
{
	public interface IPrintable
	{
		void Print();
	}
}
