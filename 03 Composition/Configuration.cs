﻿using System;
namespace InterfacesExample.Composition
{
    /// <summary>
    /// Configuration only needs to be stored, we don't care about printing
    /// </summary>
    public class Configuration : IStoreable
    {
        public Configuration()
        {
        }

        public string Id { get; set; }
        public bool FullScreen { get; set; }
		public string LastFolder { get; set; }


        public void Save()
        {
			Console.WriteLine("Configuration saved.");
			Console.WriteLine();
		}
    }
}
