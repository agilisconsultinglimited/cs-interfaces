﻿using System;
namespace InterfacesExample.Composition
{
    public class Database
    {
        public Database()
        {
        }

        public void Save(IStoreable o) {
            if (o != null) {
                o.Save();
            }
        }
    }
}
