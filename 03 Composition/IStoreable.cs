﻿using System;
namespace InterfacesExample.Composition
{
	public interface IStoreable
	{
        string Id { get; set; }

		void Save();
	}
}
