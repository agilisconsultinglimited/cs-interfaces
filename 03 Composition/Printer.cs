﻿using System;
namespace InterfacesExample.Composition
{
    public class Printer
    {
        public Printer()
        {
        }

        public void Print(IPrintable o) {
            if (o != null)
            {
                o.Print();
            }
        }
    }
}
