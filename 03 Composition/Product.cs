﻿using System;
namespace InterfacesExample.Composition
{
    /// <summary>
    /// Let's make that third-party class "ours", by wrapping it 
    /// in our own thin wrapper, which only adds ability to print and to store it.
    /// </summary>
    public class Product : IPrintable, IStoreable
    {
        public Product()
        {
        }

        public ThirdParty.Product Data { get; set; }


        public string Id { 
            get 
            {
                return Data == null ? null : Data.Code;
            }
            set {
                if (Data != null) 
                {
                    Data.Code = value;
                }
            }
        }


        public void Print()
        {
            if (Data != null)
            {
                Console.WriteLine("THIRD PARTY PRODUCT");
                Console.WriteLine("Code:     {0}", Data.Code);
                Console.WriteLine("Name:     {0}", Data.Name);
                Console.WriteLine("Quantity: {0}", Data.Quantity);
                Console.WriteLine();
            }
		}

        public void Save()
        {
            if (Data != null)
            {
                Console.WriteLine("Third party product saved.");
                Console.WriteLine();
            }
		}
    }
}
