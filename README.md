﻿# Interfaces
------------
The project aims at explaining in practical terms, what are **C# interfaces** for.
Book definitions are usually quite vague, here's an attempt to make them look useful.

Problem definition
------------------
Imagine a simple application for processing the following data entities: `Customer` and
`Employee`. It needs to be able to print them and to store them.

Because we're a nice programmer, we want to dedicate such functionality to dedicated
service classes: `Database` and `Printer`.

Database service has a single method `Save()`
Printer service has a single method `Print()`

To keep them simple, we don't want them to know anything about `Customer` or `Employee`.
We just want them to be able to print any object which is somehow printable and store
any object which is somehow storeable.



Attempt #1, classic inheritance
-------------------------------
To meet these requirements, let's introduce a base class called `DataObject`.

Let it class have two methods: `Print` and `Save`. Now, our `Printer` and `Database`
services will expect that we give them an instance of `DataObject`, and then they can
be printed or stored.

Finally, `Customer` and `Employee` classes will be derived from `DataObject`, and
override the required methods: `Print` and `Save`.

With this, I can pass both customer and employee to `Database.Save` and `Printer.Print`
methods. Both customer and employee are derived from `DataObject`, while both `Database.Save`
and `Printer.Print` expect a `DataObject` on input. And so, both customer and employee are
printed and saved.

Finally, it is easy to add new objects to our system, which are printable and saveable, without
ever touching `Printer` or `Database` services! All you need to do, is to derive your new class
from `DataObject`.



Attempt #2, with interfaces
---------------------------
One problem with the above is that we require `Customer` and `Employee` to be derived from the
same class, while ... it's just dumb. The class doesn't have any meaningful functionality
of its own. Both classes don't have much to do with each other. Why would we make them have
the same parent? We only did this, so that they both "look" similar, so that they are forced
to have methods such as Print and Save.

If this is our objective, it's much more elegant to enforce such constraint using **interfaces**.

Interface is simply a **description** of what an object can do, what methods and properties
it is expected to have. It has no implementation whatsoever.

So we defined an interface called `IDataObject` in our application. In it, we specify that if
an object is to be an IDataObject, it has to have methods Save and Print, and a property Id.

Then we tell `Employee` and `Customer` that they implement `IDataObject` interface.

In the end, we tell `Database` and `Printer` to accept any class that implements `IDataObject`
interface. Simple!

Notice, that now `Customer` and `Employee` are no longer derived from the same parent class!
They only **look** the same, as described by the interface.

> With interfaces we can also resolve another, very serious limitation of inheritance-based
> solution. The problem is the requirement from **Attempt #1**: in order to make a class printable or
> storeable, you need to derive it from `DataObject`. Well, that's fine, but what if your class
> is **already** derived from some other class? You can't derive from two classes (not in C# at
> least). Take a look at our newly introduced `Configuration` class. It is already derived
> from `List<string>`. In the previous it would not be possible to make this class storeable and
> printable using our service classes. We'd have to come up with some custom-made solution just to
> handle this special class, which would make code much less elegant and concise.

Here, with interfaces, it's easy. Hell yeah, let `Configuration` derive from `List<string>`, we
can't do much about it, if this is what our architect wants. But I can still say that it implements
`IDataObject` interface! Neat! Once I do it, I add the required methods `Save` and `Print` and ready,
we can save the configuration as well!

In the end, this isn't so much different from normal inheritance. The crucial benefit is *less
dependencies* in our class tree. This comes at a cost of slightly more code.



Attempt #3, using composition
-----------------------------
The the above attempt at interfaces has two serious shortcomings:

1) Our interface `IDataObject` is just too ... generic. Its name says nothing, unless you look
   at interface definition. And if you do, you see that it introduces two quite unrelated
   behaviours - printing and saving. We'd be better off if we separated these concerns.
2) Also, we've introduced this class `Configuration`, also implementing `IDataObject` interface.
   Our architect said that we don't need to be able to print configuration, we will only save it.
   Yet we had to implement the whole `IDataObject` interface, as the compiler requires it all.
   So we ended up having a dummy empty `Print` method, what a waste of human life.

Therefore, in another attempt at interfaces we'll split the vague `IDataObject` in two more descriptive
interfaces: `IPrintable` and `IStoreable`.

* `IPrintable` says - the object that implements me, must have a method `Print`
* `IStoreable` says - the object that implements me, must have a method `Save`, and a property called `Id`

Now we tell `Customer` and `Employee` to implement both, but we tell `Configuration` to implement only
`IStoreable`, as we don't care about printing it.

Finally, we modify our service classes. We tell database to accept anything that implements `IStoreable`.
And we tell printer to accept anything that implements `IPrintable`.

This way we can store all three classes, as they all implement `IStoreable`. And we can print two classes:
`Employee` and `Customer` as they implement `IPrintable`.

Our application is now beginning to look nice. Our interfaces clearly describe the capabilities
of the classes, and there are no dummy methods any more!


Attempt #4, Wrapping up Third Party Code
----------------------------------------
Another good use of interfaces is when our application makes use of some third-party library.
In our another attempt we introduces a class called `Product`, which comes from `SomeOneElsesLibrary`.
It's a nice class and it has all the bits and pieces, so why reinventing the wheel.

But we want to make this class printable and storeable as well. That turns out to be hard.

For once, you might have no access to the class source code. Sometimes all you have is a DLL assembly
which you add to your app.

> You might think - I can still derive from that class and implement all I need. Well, not so fast.
> Quite often (and it's a good practice) developers of reusable components mark them as `sealed`,
> to prevent inheriting from them. Why prevent? Sometimes a class is flaky, and your code added in
> a child class can cause trouble, if you don't understand the class to well and somehow mess up
> the object internal state. Sealed means - no inheritance, this is it, you get what you paid for.

Yet, with interfaces we can make it happen. All we need to do, is to create a wrapper class
which we also call `Product`. It keeps the third-party product in its `Data` property, while
it still implements the required methods of `IStoreable` and `IPrintable` interface!

A bit tricky, but it does work perfectly well and it's a commonly met design pattern.
This way you can incorporate third party code into your own, and make it **look and behave**
like it's your native components, even if infact they aren't.




