﻿using System;

namespace InterfacesExample.Inheritance
{
    public class Customer : DataObject
    {
        public Customer()
        {
        }


		public String Code { get; set; }
		public String Name { get; set; }
		public String Address { get; set; }


        public override void Save()
        {
            Console.WriteLine("Customer saved.");
            Console.WriteLine();
        }


        public override void Print()
        {
            Console.WriteLine("CUSTOMER");
            Console.WriteLine("Code:    {0}", Code);
			Console.WriteLine("Name:    {0}", Name);
			Console.WriteLine("Address: {0}", Address);
            Console.WriteLine();
		}


	}
}
