﻿using System;
namespace InterfacesExample.Inheritance
{
    public class Database
    {
        public Database()
        {
        }

        public void Save(DataObject o) {
            if (o != null) {
                o.Save();
            }
        }
    }
}
