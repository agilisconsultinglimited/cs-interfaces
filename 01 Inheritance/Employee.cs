﻿using System;

namespace InterfacesExample.Inheritance
{
	public class Employee : DataObject
	{
		public Employee()
		{
		}


		public String Name { get; set; }
		public String Address { get; set; }


		public override void Save()
		{
			Console.WriteLine("Employee saved.");
            Console.WriteLine();
		}


		public override void Print()
		{
			Console.WriteLine("EMPLOYEE");
			Console.WriteLine("Name:    {0}", Name);
			Console.WriteLine("Address: {0}", Address);
            Console.WriteLine();
		}


	}
}
