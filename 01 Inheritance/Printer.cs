﻿using System;
namespace InterfacesExample.Inheritance
{
    public class Printer
    {
        public Printer()
        {
        }

        public void Print(DataObject o) {
            if (o != null)
            {
                o.Print();
            }
        }
    }
}
