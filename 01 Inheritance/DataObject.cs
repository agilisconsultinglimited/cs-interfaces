﻿using System;

namespace InterfacesExample.Inheritance
{
    /// <summary>
    /// Generic data object
    /// </summary>
    public class DataObject
    {
		string Id { get; set; }
		
        public virtual void Save() {
            throw new NotImplementedException();
        }


		public virtual void Print()
		{
            throw new NotImplementedException();
		}

	}
}
