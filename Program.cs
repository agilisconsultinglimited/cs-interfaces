﻿using System;

namespace InterfacesExample
{
    class Program
    {
        public static void Main(string[] args)
        {
            WithInheritance();
            WithInterfaces();
            WithComposition();
            IncludingThirdPartyCode();
		}


		// Classic inheritance
		static void WithInheritance() 
        {
			Console.WriteLine("EXTENSIBILITY WITH INHERITANCE");
			Console.WriteLine(new String('-', 50));
			var customer = new InterfacesExample.Inheritance.Customer()
			{
				Name = "Microsoft Corporation",
				Code = "MSFT",
				Address = "Redmond",
			};

            var employee = new InterfacesExample.Inheritance.Employee()
			{
				Name = "Jan Doedel",
				Address = "Eindhoven",
			};

			var printer = new InterfacesExample.Inheritance.Printer();
            printer.Print(customer);
            printer.Print(employee);
			
            var database = new InterfacesExample.Inheritance.Database();
            database.Save(customer);
            database.Save(employee);

        }


		// With interfaces
		static void WithInterfaces()
		{
			Console.WriteLine("EXTENSIBILITY WITH INTERFACES");
			Console.WriteLine(new String('-', 50));
			var customer = new InterfacesExample.Interfaces.Customer()
			{
				Name = "Microsoft Corporation",
				Code = "MSFT",
				Address = "Redmond",
			};

			var employee = new InterfacesExample.Interfaces.Employee()
			{
				Name = "Jan Doedel",
				Address = "Eindhoven",
			};

            var configuration = new InterfacesExample.Interfaces.Configuration()
            {
                FullScreen = true,
                LastFolder = "C:\\DATA"
			};

			var printer = new InterfacesExample.Interfaces.Printer();
			printer.Print(customer);
            printer.Print(employee);

			var database = new InterfacesExample.Interfaces.Database();
			database.Save(customer);
			database.Save(employee);
            database.Save(configuration);

		}


		// With composition
		static void WithComposition()
		{
			Console.WriteLine("EXTENSIBILITY WITH COMPOSITION");
			Console.WriteLine(new String('-', 50));
            var customer = new InterfacesExample.Composition.Customer()
			{
				Name = "Microsoft Corporation",
				Code = "MSFT",
				Address = "Redmond",
			};

			var employee = new InterfacesExample.Composition.Employee()
			{
				Name = "Jan Doedel",
				Address = "Eindhoven",
			};

			var configuration = new InterfacesExample.Composition.Configuration()
			{
				FullScreen = true,
				LastFolder = "C:\\DATA"
			};

			var printer = new InterfacesExample.Composition.Printer();
			printer.Print(customer);
            printer.Print(employee);

			var database = new InterfacesExample.Composition.Database();
			database.Save(customer);
			database.Save(employee);
            database.Save(configuration);

		}




		// Including third-party code in your application
		static void IncludingThirdPartyCode()
		{
			Console.WriteLine("INCLUDING THIRD-PARTY CODE");
			Console.WriteLine(new String('-', 50));
            var customer = new InterfacesExample.ThirdParty.Customer()
			{
				Name = "Microsoft Corporation",
				Code = "MSFT",
				Address = "Redmond",
			};

			var employee = new InterfacesExample.ThirdParty.Employee()
			{
				Name = "Jan Doedel",
				Address = "Eindhoven",
			};

            var product = new InterfacesExample.ThirdParty.Product(
                new SomeoneElsesLibrary.Product()
				{
					Name = "Black coal",
					Code = "C0019",
					Quantity = 1200
				});

			var printer = new InterfacesExample.ThirdParty.Printer();
			printer.Print(customer);
			printer.Print(employee);
			printer.Print(product);

			var database = new InterfacesExample.ThirdParty.Database();
			database.Save(customer);
			database.Save(employee);
			database.Save(product);

		}
	}
}
